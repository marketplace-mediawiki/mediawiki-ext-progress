# Information / Информация

Интеграция шкалы прогресса завершенности задачи.

## Install / Установка

1. Загрузите папки и файлы в директорию `extensions/MW_EXT_Progress`.
2. В самый низ файла `LocalSettings.php` добавьте строку:

```php
wfLoadExtension( 'MW_EXT_Progress' );
```

## Syntax / Синтаксис

```html
{{#progress: [VALUE]|[WIDTH]}}
```

## Donations / Пожертвования

- [Donation Form](https://donation-form.github.io/)
